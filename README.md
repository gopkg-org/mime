# MIME

This package provides the MIME types constants available on [IANA] in the Go language.

[IANA]: https://www.iana.org/assignments/media-types/media-types.xhtml
